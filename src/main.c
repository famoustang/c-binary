#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define LOG_INFO  printf
#define LOG_ERROR printf


///
/// \brief isRangeChar 判断字符是否在可转换2进制的字符范围
/// \brief 包括（'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','a','b','c','d','e','f'）
/// \param c 源字符
/// \return true - 在范围 , false - 不在范围
///
int isRangeChar(char c)
{
    if((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')){
        return true;
    }else{
        return false;
    }
}

///
/// \brief char_hex2digit 字符转成数字，用数字字符表示
/// \param c 源字符
/// \return 返回字符所对应的数值
///
int char_hex2digit(char c)
{
    if(c >= '0' && c <= '9'){
        return c - '0';
    }else {
        if(c >= 'a' && c <= 'f'){
            return c - 'a' + 10;
        }else if(c >= 'A' && c <= 'F'){
            return c - 'A' + 10;
        }else{
            LOG_ERROR("Invalid char(%c)\n",c);
        }
    }
    return 0;
}

///
/// \brief longlong2bits 64bits 整型转2进制字符串
/// \param src 源数字
/// \param dest 目标字符串，64字节，需求方管理空间
///
void longlong2bits(const long long int src , char *dest)
{
    for(int i = 0 ; i < 64 ; i ++){
        dest[64 - i - 1] = (src >> i & 0x1) + '0';
    }
}

///
/// \brief int2bits 32bits 整型转2进制字符串
/// \param src 源数字
/// \param dest 目标字符串，32字节，需求方管理空间
///
void int2bits(const int src , char *dest)
{
    for(int i = 0 ; i < 32 ; i ++){
        dest[32 - i - 1] = (src >> i & 0x1) + '0';
    }
}
///
/// \brief char2bits 将一个字节的字符转成2进制，包括（'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','a','b','c','d','e','f'）
/// \param src 源字符
/// \param dest 目标字符串 ，大小4字节，需求方管理
///
void char2bits(const char src , char * dest)
{
    int i;
    for(i = 0 ; i < 8 ; i ++){
        dest[8 - i - 1] = (src >> i & 0x1) + '0';
    }
}
///
/// \brief string2bits 将字符串视为16进制，包括（'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','a','b','c','d','e','f'）转成2进制
/// \param src 数据源字符串
/// \param dest 数据输出2进制字符串，自己申请空间，自己释放，空间大小 strlen(src) * sizeof(char) * 4
///
void string2bits(const char* src ,char * dest)
{
    if(src == NULL){
        LOG_ERROR("src is NULL\n");
        return ;
    }
    if(dest == NULL){
        LOG_ERROR("dest is NULL\n");
        return ;
    }

    int len = strlen(src);
    int bits = len * 4;
    int i;
    for(i = 0 ; i < len ;  i ++){
        int tmp = char_hex2digit(src[i]);
        dest[i * 4 + 3] = ((tmp >> 0) & 0x1) + 48;
        dest[i * 4 + 2] = ((tmp >> (0 + 1)) & 0x1) + 48;
        dest[i * 4 + 1] = ((tmp >> (0 + 2)) & 0x1) + 48;
        dest[i * 4 + 0] = ((tmp >> (0 + 3)) & 0x1) + 48;
    }

}

///
/// \brief stringbits2int 2进制字符串转32位整型
/// \param src 源2进制字符串
/// \return 返回32位整型数据
///
int stringbits2int(const char* src)
{
    int i;
    int len = strlen(src);
    int d = 0;
    for(i = 0 ; i < len ; i++)
    {
        if(src[i] == '1'){
            d = d << 1 | 0x1;
        }else if(src[i] == '0'){
            d = d << 1 | 0x0;
        }else{
            LOG_ERROR("What's wrong!\n");
        }
    }

    return d;
}

///
/// \brief stringbits2longlong 2进制字符串转64位整型
/// \param src 源2进制字符串
/// \return 返回64位整型数据
///
long long int stringbits2longlong(const char* src)
{
    int i;
    int len = strlen(src);
    if(len > 64){
        LOG_ERROR("long long int max length 8 bytes!\n");
        return -1;
    }
    long long d = 0;
    for(i = 0 ; i < len ; i++)
    {
        if(src[i] == '1'){
            d = d << 1 | 0x1;
        }else if(src[i] == '0'){
            d = d << 1 | 0x0;
        }else{
            LOG_ERROR("What's wrong!\n");
        }
    }

    return d;
}

#ifdef _DEMO_TEST_
int main()
{
    long long int x = 0x123456789ABCDEF;
    char bits[64 + 1];

    memset(bits,0,sizeof(bits));
    longlong2bits(x,bits);
    LOG_INFO("0x%llX longlong2bits => %s\n",x,bits);

    int y = 0xABCDEF;
    memset(bits,0,sizeof(bits));
    int2bits(y,bits);
    LOG_INFO("0x%X int2bits => %s\n",y,bits);

    char c = 0x5C;
    memset(bits,0,sizeof(bits));
    char2bits(c,bits);
    LOG_INFO("0x%X char2bits => %s\n",c,bits);

    char *z="ABCDEF0123456789";
    char *dest = (char*)calloc(1,strlen(z) * 4 + 1);
    string2bits(z,dest);
    LOG_INFO("%s string2bits => %s\n",z,dest);
    x = stringbits2longlong(dest);
    LOG_INFO("%s stringbits2longlong => 0x%llX\n",dest,x);
    free(dest);
}
#endif
